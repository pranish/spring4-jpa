package com.example.controller;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.example.model.Product;
import com.example.service.ProductService;

import ch.qos.logback.access.pattern.RequestMethodConverter;
import io.undertow.attribute.RequestMethodAttribute;

@RestController
//@EnableWebMvc
@RequestMapping("/products")
public class MainController {
	@Autowired
	ProductService productService;

	/**Insert data **/
	@RequestMapping(value = "/", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	public Product createProduct(@ModelAttribute("product") Product product) {
		return productService.createProduct(product);
	}

	/**While using Postman, pass the update value for product_name, product_price and product_supplier through URL.
	 * PUT method doesn't take the values passed in the parameter field while using Postman
	 * **/
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json; charset=utf-8")
	public Product updateProduct(@PathVariable("id") Integer id, @ModelAttribute Product product) {
		// Product product = new Product();
		product.setProduct_id(id);
		/*product.setProduct_name("changed");
		product.setProduct_price((float) 11);
		product.setProduct_supplier("sdfsddd");*/
		System.out.println("Product id : " + product.getProduct_id() + product.getProduct_name() + product.getProduct_supplier()+ product.getProduct_price());
		
		return (productService.createProduct(product));
		// Product product
	}

	/**Delete a record by ID**/
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE, produces="application/json; charset=utf-8")
	public String deleteProduct(@PathVariable("id") Integer id){
		if(productService.deleteProduct(id)){
		return "Record has been deleted";
		}
		

		return "Unable to delete the record";
		
	}
	
	/**Select a record by ID**/
	@RequestMapping(value="/{id}", method=RequestMethod.GET, produces="application/json; charset=utf-8")
	public Product getById(@PathVariable("id") Integer id){
		
		return productService.findById(id);
	}
	
	/**List all records**/
	@RequestMapping(value="/", method=RequestMethod.GET, produces="application/json; charset=utf-8")
	public List<Product> getAll(){
		
		return (productService.findAll());
	}
}
