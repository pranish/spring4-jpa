package com.example.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.example.model.Product;


public interface ProductService {

	public Product findById(int id);

	
	public List<Product> findAll();
	
	public Product createProduct(Product product);

	public Product updateProduct(Product product);
	
	public Boolean deleteProduct(int id);
}
