package com.example.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.model.Product;
import com.example.repo.ProductRepo;

@Service
@Transactional  //Automatically makes commit and rollback
public class ProductServiceImpl implements ProductService{
	
	@Autowired
	ProductRepo productRepo;

	@Override
	public Product findById(int id) {
		// TODO Auto-generated method stub
		return productRepo.findOne(id);
	}


	@Override
	public List<Product> findAll() {
		List<Product> products = productRepo.findAll();
		return products;
	}

	@Override
	public Product createProduct(Product product) {
		// TODO Auto-generated method stub
		productRepo.save(product);
		return product;
	}

	@Override
	public Product updateProduct(Product product) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean deleteProduct(int id) {
		// TODO Auto-generated method stub
		try{
		productRepo.delete(id);
		
		return true;
		}catch(Exception e){
			return false;
		}
	}

}
