package com.example.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.model.Product;

@Repository /**Used for Exception Translation. Translates low level exceptions into higher level Spring exceptions.**/

/**This Persistence layer (JpaRepository) is introduced by Spring Data JPA. It is developed to remove all 
 * the boilerplate codes like querying to database, handling sessions and make the code simpler**/
public interface ProductRepo extends JpaRepository<Product, Integer>{
	

}
