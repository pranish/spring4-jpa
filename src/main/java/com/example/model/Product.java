package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Product")
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "product_id")
	private int product_id;

	@Column(name = "product_name")
	private String product_name;

	@Column(name = "product_supplier")
	private String product_supplier;

	@Column(name = "product_price")
	private Float product_price;

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(final int product_id) {
		this.product_id = product_id;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(final String product_name) {
		this.product_name = product_name;
	}

	public String getProduct_supplier() {
		return product_supplier;
	}

	public void setProduct_supplier(final String product_supplier) {
		this.product_supplier = product_supplier;
	}

	public Float getProduct_price() {
		return product_price;
	}

	public void setProduct_price(final Float product_price) {
		this.product_price = product_price;
	}

}

